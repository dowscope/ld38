#!/usr/bin/env python3

# Planet Wars
# Created by Tim "Smooth" Dowling
# Ludum Dare #38
# April 23, 2017

# Two small worlds are getting over populated.  The only solution is to take over the 
# neighbouring planet, which is also trying to do the same.

# --- NOTE ---  
# This is an incomplete game.  After 48 hours, I realize that I still need to practice
# and perfect my skills.  


import pygame, math, cmath, random
from pygame import *
from math import *
from random import *
from colors import *

# Declarations
width = 1000
height = 800
screen = pygame.display.set_mode(( width, height ))

# Class for creating the planets
class Planet:
	def __init__( self, x, y, radius ):
		self.x = x
		self.y = y
		self.radius = radius

		# Initialize the properties
		self.health = 100
		self.population = 100000

	def update( self ):
		# Draw the planet
		pygame.draw.circle( screen, blue, ( self.x, self.y), self.radius )

class Missle_Launcher:

	def __init__( self, x, y, planet ):
		self.planet = planet
		self.health = 100
		self.ammo = 5
		self.range = 1
		self.type = 0
		self.width = 10
		self.height = 10
		self.color = white

		# Find the angle in Radians
		self.angle = atan2((y - planet.y), (x - planet.x)) * -1 + radians(90)

		# Replot X and Y
		self.y = int((planet.y + (planet.radius * cos( self.angle ) ) ).real)
		self.x = int((planet.x + (planet.radius * sin( self.angle ) ) ).real)

		self.rect = pygame.Rect( self.x-self.width/2, self.y-self.height/2, self.width, self.height )

	def draw( self ):
		pygame.draw.rect( screen, self.color, self.rect )

class Missle:
	# Types of Missles
	# 1 - Normal Bomb

	def __init__( self, x, y, angle, missle_type, target ):
		self.x = x
		self.y = y
		self.angle = angle
		self.attack_multiplier = 1
		self.range_multiplier = 1
		self.blast_radius = 10
		self.missle_type = missle_type
		self.target = target
		self.size = 2
		self.color = white
		self.rect = pygame.Rect( self.x, self.y, self.size, self.size )
		self.hit = False

	def update( self ):
		#if self.rect.collidepoint()

		if self.y < self.target.y:
			self.y += 1
			if self.x < self.target.x:
				self.x += 1
		elif self.y > self.target.y:
			self.y -= 1
			if self.x < self.target.x:
				self.x += 1
		elif self.x > self.target.x:
			self.x -= 1
		else:
			self.x += 1

		self.rect = pygame.Rect( self.x, self.y, self.size, self.size )
		
		h = sqrt( (pow( (self.x - self.target.x), 2 )) + (pow( (self.y - self.target.y), 2 )) )

		#print(str(h) + " " + str(self.target.radius))

		if h <= self.target.radius:
			self.hit = True

	def draw( self ):
		pygame.draw.rect( screen, self.color, self.rect )


class Information_System:
	# Declare font size variables
	system_label_size = 40

	def __init__(self):
		#Initialize the font module
		pygame.font.init()

		self.lp_title = "You"
		self.rp_title = "Enemy"

	def message( self, msg, x, y, size, color ):
		this_font = pygame.font.SysFont( 'Impact', size )
		this_message = this_font.render( msg, True, color )
		screen.blit( this_message, (x, y) )	

	def update( self, lp_health, lp_population, rp_health, rp_population ):
		self.lp_hlt_str = "Health: " + str(lp_health)
		self.lp_pop_str = "Population: " + str(lp_population)
		self.rp_hlt_str = "Health: " + str(rp_health)
		self.rp_pop_str = "Population: " + str(rp_population)

	def draw( self ):
		self.message( self.lp_title, 50, height-80-self.system_label_size, self.system_label_size, green )
		self.message( self.lp_hlt_str, 50, height-80, self.system_label_size, green )
		self.message( self.lp_pop_str, 50, height-80+self.system_label_size, self.system_label_size, green )

		self.message( self.rp_title, width-300, height-80-self.system_label_size, self.system_label_size, green )
		self.message( self.rp_hlt_str, width-300, height-80, self.system_label_size, green )
		self.message( self.rp_pop_str, width-300, height-80+self.system_label_size, self.system_label_size, green )

class Button:
	def __init__( self, x, y, color ):
		self.x = x
		self.y = y
		self.width = 32
		self.height = 32
		self.color = color
		self.rect = pygame.Rect( self.x, self.y, self.width, self.height )
		self.active = False

	def update_color( self, color ):
		self.color = color

	def draw( self ):
		pygame.draw.rect( screen, self.color, self.rect )

class Enemy_manager:

	def __init__( self, difficulty, planet, target_planet ):
		self.difficulty = difficulty
		self.planet = planet
		self.target_planet = target_planet
		self.update_counter_init = 400
		self.update_counter = 0

		self.missle_launcher_array = []
		self.missle_array = []

		self.make_switch = False

	def update( self ):
		if self.update_counter >= self.update_counter_init:
			self.update_counter = 0
			angle = randrange(-180, 180)

			y = int((self.planet.y + (self.planet.radius * cos( angle ) ) ).real)
			x = int((self.planet.x + (self.planet.radius * sin( angle ) ) ).real)

			new_ml = Missle_Launcher( x, y, self.planet )

			self.make_switch = True

			for ml in self.missle_launcher_array:
				if ml.rect.colliderect( new_ml ):
					print( "Cannot create ML" )
					self.make_switch = False

			if self.make_switch:
				self.missle_launcher_array.append( new_ml )

		elif self.update_counter == self.update_counter_init / 2:
			for ml in self.missle_launcher_array:
				self.missle_array.append( Missle( ml.x, ml.y, ml.angle, 1, self.target_planet ) )

		# Update the missles
		if len( self.missle_array ) != 0:
			for m in self.missle_array:
				
				m.update()
				if m.hit:
					self.target_planet.health -= m.attack_multiplier
					self.target_planet.population -= m.blast_radius
					self.missle_array.pop( self.missle_array.index( m ) )  

		self.update_counter += 1

	def draw( self ):
		for ml in self.missle_launcher_array:
			ml.draw()

		for m in self.missle_array:
			m.draw()

def main_menu():
	screen.fill( black )

def main():
	# Initialize PYGAME
	pygame.init()

	# Fill the background of the screen
	screen.fill( black )

	# Do initial update of the screen
	pygame.display.update()

	# Initialize game loop switch
	is_running = True
	play_game = False

	# Initialize the clock
	clock = pygame.time.Clock()

	# Create the planets
	left_planet = Planet( 225, 300, 100 )
	right_planet = Planet ( width-225, height-300, 100 )

	# Create the enemy manager
	enemy_manager = Enemy_manager( 1, right_planet, left_planet )

	# Create the Information System
	info_sys = Information_System()

	# Create GUI Button for adding missle launchers
	ml_button = Button( width-100, 32, red )
	fire_button = Button( width-100, 74, red )
	exit_button = Button( width-100, 116, red )

	# Array of Missle Launchers
	missle_launcher_array = []

	# Initialize the Missle Array
	missle_array = []

	# Main Game Loop
	while is_running:

		# Check for events on the window
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				is_running = False
			elif event.type == pygame.MOUSEBUTTONDOWN:
				x,y = pygame.mouse.get_pos()

				# Check if create missle launcher button is pressed
				if ( ml_button.rect.collidepoint( x, y ) ):
					if ( ml_button.active ):
						ml_button.update_color( red )
						ml_button.active = False
					else:
						ml_button.update_color( white )
						ml_button.active = True

				elif ( ml_button.active ):
					new_ml = Missle_Launcher( x, y, left_planet )

					make_switch = True

					for ml in missle_launcher_array:
						if ml.rect.colliderect( new_ml ):
							print( "Cannot create ML" )
							make_switch = False

					if make_switch:
						missle_launcher_array.append( new_ml )

				# Check if fire button is pressed
				elif ( fire_button.rect.collidepoint( x, y ) ):
					fire_button.active = True
					fire_button.update_color( white )

					if len(missle_launcher_array) != 0:
						for ml in missle_launcher_array:
							missle_array.append( Missle( ml.x, ml.y, ml.angle, 1, right_planet ) )
							ml.ammo -= 1
							if ml.ammo <= 0:
								missle_launcher_array.pop(missle_launcher_array.index(ml))

					fire_button.active = False
					fire_button.update_color( red )

		#main_menu()

		# Main Game Play Scene
		
		# Update Planets
		left_planet.update()
		right_planet.update()

		# Update the missles
		if len( missle_array ) != 0:
			for m in missle_array:
				m.update()
				if m.hit:
					right_planet.health -= m.attack_multiplier
					right_planet.population -= m.blast_radius
					missle_array.pop( missle_array.index( m ) ) 


		# Update the Information System
		info_sys.update( left_planet.health, left_planet.population, \
			right_planet.health, right_planet.population )

		# Update the enemies
		enemy_manager.update()

		# Draw Information System
		info_sys.draw()

		# Draw GUI buttons
		ml_button.draw()
		fire_button.draw()
		#exit_button.draw()

		# Draw Missle Launchers
		for ml in missle_launcher_array:
			ml.draw()

		# Draw Missles
		for m in missle_array:
			m.draw()

		# Draw the Enemies
		enemy_manager.draw()

		# Update the screen
		pygame.display.update()
		screen.fill( black )
		clock.tick(60)

	pygame.quit()



if __name__ == "__main__":
	main()