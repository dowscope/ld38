#!/usr/bin/env/ python3
black = ( 0, 0, 0 )
white = ( 255, 255, 255 )
red = ( 255, 0, 0 )
green = ( 0, 255, 0 )
blue = ( 0, 0, 255 )
sky_blue = ( 82, 171, 255 )
dirt_brown = ( 128, 102, 44 )